package com.bsa.springdata.office;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class OfficeService {
    @Autowired
    private OfficeRepository officeRepository;

    public List<OfficeDto> getByTechnology(String technology) {
        return officeRepository.getByTechnology(technology);
    }

    public Optional<OfficeDto> updateAddress(String oldAddress, String newAddress) {
        return officeRepository.updateAddress(oldAddress, newAddress) == 0
                ? Optional.empty()
                : officeRepository.findByAddress(newAddress).map(OfficeDto::fromEntity);
    }
}
