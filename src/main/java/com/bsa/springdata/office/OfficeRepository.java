package com.bsa.springdata.office;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface OfficeRepository extends JpaRepository<Office, UUID> {

    // Explicit inner join: https://www.baeldung.com/jpa-join-types
    // Constructor expression: https://docs.oracle.com/cd/E12839_01/apirefs.1111/e13946/ejb3_langref.html#ejb3_langref_constructor
    // Using GROUP BY instead of DISTINCT since it doesn't work with constructor expression
    @Query("""
      SELECT NEW com.bsa.springdata.office.OfficeDto(o.id, o.city, o.address)
        FROM Office o
        JOIN o.users u
        JOIN u.team tm
        JOIN tm.technology tech
       WHERE tech.name = ?1
       GROUP BY o.id
    """)
    List<OfficeDto> getByTechnology(String technology);

    // Empty collection comparison: https://docs.oracle.com/cd/E12839_01/apirefs.1111/e13946/ejb3_langref.html#ejb3_langref_empty_comp
    @Transactional
    @Modifying
    @Query("""
      UPDATE Office o
         SET o.address = ?2
       WHERE o.address = ?1
         AND o.users IS NOT EMPTY
    """)
    int updateAddress(String oldAddress, String newAddress);

    Optional<Office> findByAddress(String address);
}
