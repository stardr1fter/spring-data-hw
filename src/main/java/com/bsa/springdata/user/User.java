package com.bsa.springdata.user;

import com.bsa.springdata.office.Office;
import com.bsa.springdata.role.Role;
import com.bsa.springdata.team.Team;
import com.bsa.springdata.user.dto.CreateUserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Set;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name = "users") // default would be "User"
public class User {
    @Id // mark as primary key
    @GeneratedValue(generator = "UUID")
    // Primary key generation strategy
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(updatable = false, nullable = false)
    private UUID id;

    private String firstName;

    private String lastName;

    private int experience;

    @ManyToOne(
            cascade = CascadeType.REFRESH, // refresh: when User is refreshed, User.office refreshes as well
            fetch = FetchType.LAZY) // lazy: make additional request when accessing property for the first time
    @JoinColumn(name = "office_id") // foreign key name (same as field name by default)
    private Office office;


    @ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
    @JoinColumn(name = "team_id")
    private Team team;

    @ManyToMany(cascade = {
            CascadeType.PERSIST, // persist: when User is saved, user-role mapping gets saved as well
            CascadeType.MERGE // do the same when performing merge operation
    }, fetch = FetchType.LAZY)
    @JoinTable(
            name = "user2role",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;

    public static User fromDto(CreateUserDto user, Office office, Team team) {
        return User.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .experience(user.getExperience())
                .office(office)
                .team(team)
                .build();
    }
}


