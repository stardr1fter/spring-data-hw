package com.bsa.springdata.user;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    List<User> findByLastNameContainingIgnoreCase(String lastName, Pageable pageable);

    @Query("SELECT u FROM User u WHERE u.office.city = ?1")
    List<User> findByCity(String city, Sort sort);

    List<User> findByExperienceGreaterThanEqualOrderByExperienceDesc(int experience);

    @Query("SELECT u FROM User u WHERE u.team.room = ?1 AND u.office.city = ?2")
    List<User> findByRoomAndCity(String room, String city, Sort sort);

    int deleteByExperienceLessThan(int experience);
}
