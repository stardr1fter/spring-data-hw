package com.bsa.springdata.team;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;
    @Autowired
    private TechnologyRepository technologyRepository;

    public void updateTechnology(int devsNumber, String oldTechnologyName, String newTechnologyName) {
        var newTechnology = technologyRepository.findByName(newTechnologyName);
        if (newTechnology.isEmpty()) {
            return;
        }

        var teams = teamRepository.findByTechnologyAndDevsNumberLessThan(oldTechnologyName, devsNumber);
        teams.forEach(t -> t.setTechnology(newTechnology.get()));
        teamRepository.saveAll(teams);
    }

    public void normalizeName(String name) {
        teamRepository.normalizeName(name);
    }
}
