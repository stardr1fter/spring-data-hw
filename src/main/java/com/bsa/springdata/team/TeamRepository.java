package com.bsa.springdata.team;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TeamRepository extends JpaRepository<Team, UUID> {
    Optional<Team> findByName(String teamName);

    int countByTechnologyName(String newTechnology);

    // Arithmetic functions, count collection elements: https://docs.oracle.com/middleware/11119/wls/KDJJR/ejb3_langref.html#ejb3_langref_arithmetic
    @Query("""
      SELECT tm
        FROM Team tm
       WHERE tm.technology.name = ?1
         AND SIZE(tm.users) < ?2
    """)
    List<Team> findByTechnologyAndDevsNumberLessThan(String technologyName, int devsNumber);

    @Transactional
    @Modifying
    @Query(value = """
         UPDATE teams tmOrig
            SET name = CONCAT(tm.name, '_', p.name, '_', tech.name)
           FROM teams tm
      LEFT JOIN projects p ON tm.project_id = p.id
      LEFT JOIN technologies tech ON tm.technology_id = tech.id
          WHERE tm.name = ?1 AND tm.id = tmOrig.id
    """, nativeQuery = true)
    void normalizeName(String name);
}
