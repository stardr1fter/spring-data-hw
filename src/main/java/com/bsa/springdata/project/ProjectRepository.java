package com.bsa.springdata.project;

import com.bsa.springdata.project.dto.ProjectSummaryDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    @Query("""
      SELECT p, SUM(SIZE(tm.users)) as userCount
        FROM Project p
        JOIN p.teams tm
       WHERE tm.technology.name = ?1
       GROUP BY p
       ORDER BY userCount DESC
    """)
    List<Project> findTop5ByTechnology(String technology, Pageable page);

    @Query("""
        SELECT p, SUM(SIZE(tm.users)) as userCount
          FROM Project p
          JOIN p.teams tm
      GROUP BY p
      ORDER BY SIZE(p.teams) DESC,
               userCount DESC,
               p.name DESC
    """)
    List<Project> findTheBiggest(Pageable page);


    @Query("""
      SELECT COUNT(DISTINCT p.id)
        FROM Project p
        JOIN p.teams tm
        JOIN tm.users u
        JOIN u.roles r
       WHERE r.name = ?1
    """)
    int getCountWithRole(String role);

    @Query(value = """
          SELECT p.name,
                 COUNT(DISTINCT tm.id) AS teamsNumber,
                 COUNT(DISTINCT u.id) AS developersNumber,
                 string_agg(DISTINCT tech.name, ',') AS technologies
            FROM projects p
      INNER JOIN teams tm ON tm.project_id = p.id
      INNER JOIN technologies tech ON tm.technology_id = tech.id
      INNER JOIN users u ON u.team_id = tm.id
        GROUP BY p.name
    """, nativeQuery = true)
    List<ProjectSummaryDto> getSummary();
}